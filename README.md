# TechuBank commons

Set of facilities used by the different TechuBank's components.

*  [Environments](#environments)
*  [Logger traces](#logger)
*  [Generic Http Request Handler](#http_request_handler)
*  [Mongoose utils to interact with a MongoDB](#mongooseUtils)
*  [Security checks](#security_checks)
*  [Encrypt and decrypt string](#crypt)
*  [Enable CORS](#enable_cors)

Let's explain some of them.

(#environments)
# Environments

Common environment variables shared by all the components depending the environment (dev, docker, kubernetes, local, prod)
The environment value es loaded from the environmente variable ENV.

*How to use it*
The way to use for loading other settings is loading the environment variable ENV:
```
$ ENV=docker npm run start
```

In your javascript file:
```
const { environment } = require('techubank-commons')

console.log(environment.value)
```

It returns:
```
environment = {
            production: false,
            branch: 'develop',
            enableCors: true,
            protocol: 'http',
            techubank_api_url: 'http://localhost:3000',
            techubank_mlab_url: 'http://localhost:5000',
            techubank_auth_server_url: 'http://localhost:4000'
        }
```

(#logger)
# Logger
Logger utils with the library [Winston](https://github.com/winstonjs/winston).

Settings are hardcoded right know. 

```
// fetch de logger
    { logger } = require('techubank-commons')

// and use it
    logger.info(`App listening HTTPS on port ${port_ssl}`)

```

(#http_request_handler)
# Generic Http Request Handler

It tries to manage in a generic way any http response depending the response statusCode.

*Handlers object*
```
  	{
 	success_handler: methodName_success_handler,            // statusCode = 200
 	error_handler: methodName_error_handler,                // statusCode = 400
 	unauthorized_handler: methodName_unauthorized_handler,    // statusCode = 401
 	forbidden_handler: methodName_forbidden_handler,        // statusCode = 403
 	server_error_handler: methodName_server_error_handler   // statusCode = 500
  }
```

*How to use it*

```
 { http_request_handler } = require('techubank-commons')

...
    return this.client.put('/v1/user/resetPassword', user, function (err_put_mlab, res_put_mlab) {
        return http_request_handler(err_put_mlab, res_put_mlab, {
            success_handler: _resetPassword_success_handler,
            error_handler: _resetPassword_error_handler
        }, res)
    })
    
....

function _resetPassword_success_handler(err, res, callback) {
    return callback.send('Password reset')
}

function _resetPassword_error_handler(err, res, callback) {
    return callback.send('ERROR reseting password!!!! ')
}
```

(#mongooseUtils)
# Mongoose utils to interact with a MongoDB

Minimum set of enough functions for storing a Mongoose Model into a MongoDB Database.

Operatives:
- Find one element 
- Find all results given a filter
- Find one element and update it
- Create one element
- Delete one element

```
// import the utils
const {
	mongooseUtils
} = require('techubank-commons'),

...

// import a Mongoose Model
	{
		user
	} = require('../models/user'),

...

// use it
    const filter = {
        $or: [{
            username: req.body['client_id']
        }, {
            email: req.body['client_id']
        }, {
            nif: req.body['client_id']
        }]
    }
    
	mongooseUtils.findOne(user, filter).then((user_found) => {
			if (crypt.checkpassword(req.body['client_secret'], user_found.password)) {
				const new_token = new token()

				new_token.access_token = crypt.hash('YOU ARE GRANTED TO ACCESS THE API!!!!')
	....


```

Schema example
```
const mongoose = require('mongoose')
	Schema = mongoose.Schema,
	{AddressSchema} = require('./address'),
	UserSchema = new Schema({
		username: {
			type: String,
			required: true
		},
		name: {
			type: String,
			required: true
		},		
		first_name: {
			type: String,
			required: true
		},
		last_name: {
			type: String
		}
	})

UserSchema.index({
	username: 1
}, {
	unique: true
}) 

module.exports.UserSchema = UserSchema
module.exports.user = mongoose.model('user', UserSchema)
```

(#security_checks)
# Security checks

This utility fetchs a valid token for BOT users and communicate with the auth server to request information about a given token.


# Development

## Requirements
This project needs:
- Nodejs 8.4+

## Folders

- bin: helpers
	- pre-commit: execute linter and unit tests before a commit
- src: The logic
- test: unit tests folder

## Compilation
Stay on the base path where package.json is located.

```
techu@techu> npm install

```

## Static code analysis

### Linter
Linter is a light weight static code analysis recomended to be execute in every push to a branch, and may block the push.
```
techu@techu> npm run lint

```

## Sonarqube - sonar-scanner Docker image
We have prepared a container with the software enough for run a static code analysis, execute unit tests, measure unit test coverage and upload the results to Sonarqube (free account). 

You must have a Sonarqube API Key, configure properly the sonar-scanner.properties and run the command below:
```
$ docker run -it --env SONAR_API_KEY=<SONAR_USER_API_KEY> --mount type=bind,source="$(pwd)",target=/root/project -v "$(pwd)"/sonar-scanner.properties:/root/sonar-scanner/conf/sonar-scanner.properties davidrova/sonar-scanner:latest
```

To see the result [click on this link](https://sonarcloud.io/projects).

>
> The evaluation period has expired and we cannot upload more executions.
>

Don't worry, you can still perform static code analysis, but following a couple of additional steps.

## Sonarqube - sonar Docker image

1) Launch a sonarqube instance (admin / admin)
```
$ docker run -d -p 9000:9000 -p 9092:9092 sonarqube
```

2) Generate a new API-KEY

3) Download sonar-scanner in localhost or use sonar-scanner Docker image
    [sonar-scanner documentation](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner)

4) Run unit tests and coverage
```
$ npm run test
$ npm run coverage
```

5) Run sonar-scanner; sonar-scanner loads sonar-project.properties file
```
$ SONAR_API_KEY=<SONAR_API_KEY> sonar-scanner
```

6) View the results in [Sonarqube](http://localhost:9000)




## Hooks
We encourage to enable the pre-commit hook. It will avoid commiting a change if linter or unit tests fail.

```
techu@techu> cp bin/pre-commit .git/hooks/pre-commit

```

## Continuous integration
The project is managed in bitbucket.org and we have automated an ease pipeline with the following step rules:

- A commint into *develop* branch triggers: 
	- build
	- Increases the *patch version*
	- Publish the artifact to npmrc registry
- A commit into *master* branch triggers:
	- build
	- lint
	- unit tests
	- Increases the *minor version*
	- Publish the artifact to npmrc registry


NOTE: It's neccessary to generate a new key pair. [Follow these instructions](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html?_ga=2.62235695.2086604014.1539165652-1490061868.1526909294)

