const { environment } = require('./environments/environment'),
    { crypt } = require('./utils/crypt'),
    { enable_cors } = require('./utils/enable_cors'), 
    { logger } = require('./utils/logger'),
    { mongooseUtils } = require('./utils/mongoose_utils'),
    { get_admin_token, check_security_headers, check_security_headers_admin_role } = require('./utils/security_checks'),
    { http_request_handler, error_message, propagate_error_response } = require('./utils/http_request_handler')


module.exports.environment = environment
module.exports.crypt = crypt
module.exports.enable_cors = enable_cors
module.exports.get_admin_token = get_admin_token
module.exports.logger = logger
module.exports.mongooseUtils = mongooseUtils
module.exports.check_security_headers = check_security_headers
module.exports.check_security_headers_admin_role = check_security_headers_admin_role
module.exports.http_request_handler = http_request_handler
module.exports.error_message = error_message
module.exports.propagate_error_response = propagate_error_response