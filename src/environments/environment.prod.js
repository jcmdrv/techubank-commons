exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'https',
    techubank_web_url: 'http://techubank-web:8001',    
    techubank_api_url: 'https://techubank-api:3000',
    techubank_mlab_url: 'https://techubank-mlab:5000',
    techubank_auth_server_url: 'https://techubank-auth-server:4000'
}
