exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'http',
    techubank_web_url: 'http://localhost:8001',    
    techubank_api_url: 'http://localhost:3000',
    techubank_mlab_url: 'http://localhost:5000',
    techubank_auth_server_url: 'http://localhost:4000'
}
