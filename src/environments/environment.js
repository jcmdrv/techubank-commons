const {
    logger
} = require('../utils/logger')

class environment {
    constructor() {
        this.env = this.get_env()
        this.value = this.get_value()
        logger.info(`Properties from environment.${this.env} loaded`)
    }

    get_env() {
        return (process.env.ENV) ? process.env.ENV : 'local'
    }

    get_value() {
        const {
            environment
        } = require(`./environment.${this.env}`)
        return environment
    }
}


module.exports.environment = new environment()