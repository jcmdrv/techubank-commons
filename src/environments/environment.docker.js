exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'http',
    techubank_web_url: 'http://techubank-web:8001',        
    techubank_api_url: 'http://techubank-api:3000',
    techubank_mlab_url: 'http://techubank-mlab:5000',
    techubank_auth_server_url: 'http://techubank-auth-server:4000'
}
