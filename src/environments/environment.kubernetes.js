exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'https',
    techubank_web_url: 'https://techubank-web-service:8443',    
    techubank_api_url: 'https://techubank-api-service:3443',
    techubank_mlab_url: 'https://techubank-mlab-service:5443',
    techubank_auth_server_url: 'https://techubank-auth-server-service:4443'
}
