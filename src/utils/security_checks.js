const
    { logger } = require('./logger'),
    { environment } = require('../environments/environment'),
    requestJson = require('request-json'),
    oauth_client = requestJson.createClient(`${environment.value.techubank_auth_server_url}`)


/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
function get_admin_token() {
    return new Promise((resolve, reject) => {
        logger.info('Get and admin token')
        return oauth_client.get(`/oauth/authorize?response_type=code&client_id=${process.env.API_USERNAME}&redirect_uri=&scope=admin&state=1234567890`, (auth_err, auth_res) => {
            if (auth_err) {
                logger.error(auth_err)
                return reject(auth_err)
            } 

            const token_body = {
                'grant_type': 'authorization_code',
                'code': auth_res.body.code,
                'client_id': process.env.API_USERNAME,
                'client_secret': process.env.API_PASSWORD,
                'redirect_uri': ''
            }

            logger.info('Request a new token')
            return oauth_client.post(`/oauth/token?response_type=code&client_id=${process.env.API_USERNAME}&redirect_uri=&scope=admin&state=1234567890`, token_body, (token_err, token_resp) => {
                if (token_err) {
                    return reject(token_err)
                }
                logger.info('### TOKEN RECEIVED ####')
                resolve(token_resp.body)
            })
        })

    })
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function check_security_headers(req, res, next, scope = 'user') {
    // always allow OPTIONS
    if (req.method == 'OPTIONS') {
        return next()
    }
    
    if (req.headers['authorization']) {
        const token_to_check = {
            target_token: req.headers.authorization.replace('Bearer', '').trim(),
            target_scope: scope
        }

        return get_admin_token().then((token) => {
            logger.info(`Admin token fetched ${JSON.stringify(token)}`)

            oauth_client.headers['authorization'] = `${token.token_type} ${token.access_token}`
            return oauth_client.post('/va/check-token', token_to_check, (err, resp) => {
                if (resp.statusCode === 200) {
                    return next()
                } else {
                    return res.status(401).send({
                        internal_code: 'invalid-token',
                        message: 'Error validating token'
                    }) 
                }
            })
        
        }).catch(() => {
            logger.error('Fail getting token')
        })

    } else {
        return res.status(401).send('Unauthorized: you must be logged before doing any request')
    }
}


function check_security_headers_admin_role (req, res, next) {
    return check_security_headers(req, res, next, 'admin')    
}

module.exports.check_security_headers = check_security_headers
module.exports.check_security_headers_admin_role = check_security_headers_admin_role
module.exports.get_admin_token = get_admin_token
