/**
 * 
 * @param {object} err This is the error object receive by an incorrect http request
 * @param {object} res This is the response object to be sent to the requestor. This is the object to use to finish the call.
 * @param {object} handlers A json with all the possible handlers managed by our application (error 200, 400, 403, 401, 500). Let's stablish a naming convention for those function names ( methodName_error_handler, ....)
 * 	{
 *	success_handler: methodName_success_handler,
 *	error_handler: methodName_error_handler,
 *	unauthorized_handler: methodName_unauthorized_handler,
 *	forbidden_handler: methodName_forbidden_handler,
 *	server_error_handler: methodName_server_error_handler
 * }
 * @param {object} callback This is the error object receive by an incorrect http request
 * @param {object} ...additional_args Aditional arguments
 */
function http_request_handler (err, res, handlers, callback, ...additional_args) {
    switch (res.statusCode) {
    case 200:
        return handlers.success_handler(err, res, callback, ...additional_args)
    case 400:
        return handlers.error_handler(err, res, callback, ...additional_args)
    case 401:
        return handlers.unauthorized_handler(err, res, callback, ...additional_args)
    case 403:
        return handlers.forbidden_handler(err, res, callback, ...additional_args)
    case 404:
        return handlers.notfound_handler(err, res, callback, ...additional_args)
    case 500:
        return handlers.server_error_handler(err, res, callback, ...additional_args)
    default:
        return callback.send('statusCode not managed')
    }
}

/**
 * 
 * @param {*} code 
 * @param {*} msg 
 */
function error_message (code, msg) {
    return {
        internal_code: code,
        message: msg
    }
}

/**
 * 
 * @param {*} code 
 * @param {*} msg 
 * @param {*} statusCode statusCode to propagate 
 * @param {*} callback 
 */
function propagate_error_response (code, msg, statusCode, callback) {
    return new Promise((resolve, reject) => {
        try {
            const message_sent = error_message(code, msg) 
            callback.statusCode = statusCode
            callback.send(message_sent)
            resolve(message_sent)
        } catch (error) {
            reject(error)
        }
    })
}

module.exports.http_request_handler = http_request_handler
module.exports.error_message = error_message
module.exports.propagate_error_response = propagate_error_response