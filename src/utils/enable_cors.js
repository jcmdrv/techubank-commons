/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next If the function is called from a Router as a filter, we finish calling the callback function
 */
function enable_cors (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE')
    res.setHeader('Access-Control-Allow-Headers', '*')
    res.setHeader('Access-Control-Allow-Credentials', 'true')
    
    if (next){
        next()
    }
}

module.exports.enable_cors = enable_cors
