const {
    logger
} = require('./logger')

module.exports.mongooseUtils = {

    /**
	 * Given a mongoose model with a mongo db connection open this function returns 1 result for a given filter.
	 * 
	 * @param {*} model Mongoose model
	 * @param {*} filter A filter json
	 */
    findOne(model, filter) {
        logger.info(`Find One: ${JSON.stringify(filter)}`)

        return new Promise((resolve, reject) => {
            return model.findOne(filter).then((document_found) => {
                if(document_found){
                    logger.debug('Document found')
                    logger.debug(JSON.stringify(document_found))
                    resolve(document_found)
                } else {
                    logger.debug('Document not found')
                    reject('Document not found')
                }
            }).catch((error) => {
                logger.error(`Error searching the document ${error}`)
                reject('Document not found')
            })
        })
    },

    /**
	 * 
	 * Given a mongoose model with a mongo db connection open this function returns all occurrences which match the filter.
	 * 
	 * @param {*} model Mongoose model
	 * @param {*} filter A filter json
     * @param {*} order The sort order json
	 */
    findAll(model, filter, order) {
        logger.info(`Find All ${JSON.stringify(filter)}`)

        return new Promise((resolve, reject) => {
            return  model.find(filter).sort(order).then((document_found) => {
                if(Array.isArray(document_found) && document_found.length > 0){
                    logger.debug('Document/s found')
                    logger.debug(JSON.stringify(document_found))
                    resolve(document_found)
                } else {
                    logger.debug('Document/s not found')
                    reject('Document/s not found')
                }
            }).catch((error) => {
                logger.error(`Error searching the document/s ${error}`)
                reject('Document/s not found')
            })
        })
    },

    /**
	 * 
	 * Given a mongoose schema
	 * I want to update THE ONLY RESULT which matches my filter
	 * And replace the document with the update I pass as argument
	 * 
	 * @param {*} modelSchema 
	 * @param {*} filter 
	 * @param {*} update 
	 */
    findOneAndUpdate(model, filter, update){
        logger.info(`Find and update ${JSON.stringify(filter)}`)

        return new Promise((resolve, reject) => {
            return model.findOneAndUpdate(filter, update, {new:true}, (err, doc) => {
                if(doc){
                    logger.info('Document/s found and updated')
                    logger.debug(JSON.stringify(doc))
                    resolve(doc)
                } else {
                    logger.info('Document/s not found')
                    reject('Document/s not found')
                }
            }).catch((error) => {
                logger.error(`Error searching the document ${error}`)
                reject('Document not found')
            })
        })
    },

    /**
	 * 
	 * Persist the document into MongoDB
	 * 
	 * @param {*} newDocument Mongoose model
	 */
    save(newDocument) {
        logger.info('Saving new Document')
        return new Promise((resolve, reject) => {
            newDocument.save(function (err, saved_document) {
                if (err) {
                    reject(`Error saving model in Mongo: ${err}`)
                } else {
                    resolve(saved_document)
                }
            })
        })
    },

    /**
	 * 
	 * Given a mongoose schema
	 * I want to update THE ONLY RESULT which matches my filter
	 * And replace the document with the document I pass as argument
	 * 
	 * This function works only with plain documents. If you need to work with subdocuments use another function
	 * @param {*} modelSchema 
	 * @param {*} filter 
	 * @param {*} document 
	 */
    update(modelSchema, filter, document) {
        logger.info(`Update Document ${JSON.stringify(filter)}`)
        return new Promise((resolve, reject) => {
            const options = {
                overwrite: true
            }

            return modelSchema.updateOne(filter, document, options, function (err, saved_document) {
                if (err) {
                    reject(`Error updating document in Mongo: ${err}`)
                } else {
                    resolve(saved_document)
                }
            })
        })
    },

    /**
	 * Deleting one element from a given collection
	 * 
	 * @param {object} model 
	 * @param {object} filter 
	 */
    deleteOne(model, filter) {
        logger.info(`Deleting Document ${JSON.stringify(filter)}`)
        return new Promise((resolve, reject) => {
            return model.deleteOne(filter, function (err, saved_document) {
                if (err) {
                    reject(`Error deleting document in Mongo: ${err}`)
                } else {
                    resolve(saved_document)
                }
            })
        })	
    }
}