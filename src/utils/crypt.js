const Crypto = require('node-crypt'),
    crypto = new Crypto({
        key: 'b95d8cb128734ff8821ea634dc34334535afe438524a782152d11a5248e71b01',
        hmacKey: 'dcf8cd2a90b1856c74a9f914abbb5f467c38252b611b138d8eedbe2abb4434fc'
    })

function hash(data) {
    return crypto.encrypt(data)
}

function checkpassword(sentPassword, userHashPassword) {
    return sentPassword == crypto.decrypt(userHashPassword)
}

module.exports.hash = hash
module.exports.checkpassword = checkpassword
module.exports.crypt = {
    checkpassword,
    hash
}
