/* global describe it */
const assert = require('assert'),
    { error_message, propagate_error_response } = require('../src/utils/http_request_handler')


describe('Testing http_request_handler', function () {


    it('Get error_message', function (done) {
        const expected_result = {
            internal_code: 'code',
            message: 'Dummy Message'
        }

        assert.equal(JSON.stringify(error_message('code', 'Dummy Message')), JSON.stringify(expected_result))
        done()
    })

    it('Propagate error message', function (done) {
        const res = {
                statusCode: 500,
                body: 'This is an error'
            },
            callback = {
                send: () => {
                    return 0
                }
            },
            dummy_message = {
                internal_code: 'code',
                message: 'Dummy Message'
            }
            
        propagate_error_response('code', 'Dummy Message', res.statusCode, callback).then((result) => {
            assert.equal(JSON.stringify(result), JSON.stringify(dummy_message))
            done()
        })
    })
})