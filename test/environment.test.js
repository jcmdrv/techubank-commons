/* global describe it afterEach */
const assert = require('assert'),
    sinon = require('sinon')


describe('Testing User environment', function () {

    afterEach(() => {
        // Restore the default sandbox here
        sinon.restore()
    })

    it('Get default environment', function (done) {
        const {
                environment
            } = require('../src/index'),
            default_environment = {
                production: false,
                branch: 'develop',
                enableCors: true,
                protocol: 'http',
                techubank_web_url: 'http://localhost:8001',   
                techubank_api_url: 'http://localhost:3000',
                techubank_mlab_url: 'http://localhost:5000',
                techubank_auth_server_url: 'http://localhost:4000'
            }

        assert.equal(JSON.stringify(environment.value), JSON.stringify(default_environment))
        done()
    })


    it('Get environment depending the ENV KUBERNETES environment variable', function (done) {        
        const {
                environment
            } = require('../src/environments/environment'),
            default_environment = {
                production: false,
                branch: 'develop',
                enableCors: true,
                protocol: 'https',
                techubank_web_url: 'https://techubank-web-service:8443',
                techubank_api_url: 'https://techubank-api-service:3443',
                techubank_mlab_url: 'https://techubank-mlab-service:5443',
                techubank_auth_server_url: 'https://techubank-auth-server-service:4443'
            }

        let stub = sinon.stub(environment, 'get_env')

        stub.returns('kubernetes')
    
        environment.env = environment.get_env()

        assert.equal(JSON.stringify(environment.get_value()), JSON.stringify(default_environment))
        done()
    })


    it('Get environment depending the ENV environment variable', function (done) {        
        const {
                environment
            } = require('../src/environments/environment'),
            default_environment = {
                production: false,
                branch: 'develop',
                enableCors: true,
                protocol: 'http',
                techubank_web_url: 'http://localhost:8001',
                techubank_api_url: 'http://localhost:3000',
                techubank_mlab_url: 'http://localhost:5000',
                techubank_auth_server_url: 'http://localhost:4000'
            }
        
        let stub = sinon.stub(environment, 'get_env')
        stub.returns('local')

        environment.env = environment.get_env()

        assert.equal(JSON.stringify(environment.get_value()), JSON.stringify(default_environment))
        done()
    })

})